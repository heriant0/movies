const axios = require('axios')
const moment = require("moment-timezone")
const { log } = require('../db/models')

async function saveLog(params) {

  await log.create(params);

  return
}


const MovieServices = {

  searchMovies: async (title, urlPath) => {

    try {
      // save log
      await saveLog({ path: urlPath, access_date: moment() })

      let result = []
      const data = await axios.get(`https://www.omdbapi.com/?apikey=35bb6dd8&s=${title}`)
      result = data.data.Search

      return result

    } catch (e) {
      console.log(e.message)
      throw new Error(e.message)
    }
  },

  detailMovies: async (imdbID, urlPath) => {

    try {
      // save log
      await saveLog({ path: urlPath, access_date: moment() })

      const data = await axios.get(`https://www.omdbapi.com/?apikey=35bb6dd8&i=${imdbID}`)
      let result = data.data

      if (result.Response == "False") {
        return false
      } else {
        return result
      }


    } catch (e) {
      console.log(e)
      throw new Error(e.message)
    }
  },

}

module.exports = MovieServices
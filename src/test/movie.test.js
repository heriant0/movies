const request = require('supertest');
const app = require('../../app')

describe("GET /movies/search", () => {

  const params = {
    title: "spiderman",
  }

  const expected = [{
    "Title": "Italian Spiderman",
    "Year": "2007",
    "imdbID": "tt2705436",
    "Type": "movie",
    "Poster": "https://m.media-amazon.com/images/M/MV5BYjFhN2RjZTctMzA2Ni00NzE2LWJmYjMtNDAyYTllOTkyMmY3XkEyXkFqcGdeQXVyNTA0OTU0OTQ@._V1_SX300.jpg"
  }]

  /**
 * Testing Search Movies
 * request params : title : Batman, page : 2
 * response: 200
 * response json : { code : 200, message: "Succesfully get data", data: array}
 */
  test("200 - Successfully get data", (done) => {

    request(app)
      .get(`/movies/search?title=${params.title}`)
      .send()
      .set('Accept', 'application/json')
      .then(response => {
        const { body, status } = response
        expect(status).toBe(200)
        expect(body).toHaveProperty("code", 200,)
        expect(body).toHaveProperty("message", "Succesfully get data")
        expect(body.data).toEqual(expect.arrayContaining(expected))

        done()
      })
      .catch(e => done(e))
  })

  /**
 * Testing Search Movies
 * request params : title : Batman, page : 2
 * response: 400
 * response json : { code : 200, message: "Data not found", data: []}
 */
  test("400 - Data not found", (done) => {

    request(app)
      .get(`/movies/search?title=asdfasdf`)
      .send()
      .set('Accept', 'application/json')
      .then(response => {
        const { body, status } = response
        expect(status).toBe(200)
        expect(body).toHaveProperty("code", 400,)
        expect(body).toHaveProperty("message", "Data not found")
        expect(body).toHaveProperty("data", null)

        done()
      })
      .catch(e => done(e))
  })
})


describe("GET /movies/detail", () => {
  const params = {
    imdbID: "tt0100669",
  }

  const expected = {
    "Title": "Spiderman",
    "Year": "1990",
    "Rated": "N/A",
    "Released": "N/A",
    "Runtime": "5 min",
    "Genre": "Short",
    "Director": "Christian Davi",
    "Writer": "N/A",
    "Actors": "N/A",
    "Plot": "N/A",
    "Language": "German",
    "Country": "Switzerland",
    "Awards": "N/A",
    "Poster": "N/A",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.6/10"
      }
    ],
    "Metascore": "N/A",
    "imdbRating": "5.6",
    "imdbVotes": "96",
    "imdbID": "tt0100669",
    "Type": "movie",
    "DVD": "N/A",
    "BoxOffice": "N/A",
    "Production": "N/A",
    "Website": "N/A",
    "Response": "True"
  }

  /**
 * Testing Detail Movie
 * request params : imdbID: "tt1673430", title : "Batman", page : 2
 * response: 200
 * response json : { code : 200, message: "Succesfully get data", data: object}
 */

  test("200 - Successfully get data", (done) => {
    request(app)
      .get(`/movies/detail?imdbID=${params.imdbID}`)
      .send()
      .set('Accept', 'application/json')
      .then(response => {
        const { body, status } = response
        expect(status).toBe(200)
        expect(body).toHaveProperty("code", 200,)
        expect(body).toHaveProperty("message", "Succesfully get data")
        expect(body).toHaveProperty("data", expected)

        done()
      })
      .catch(e => {
        done(e)
      })
  })

  /**
   * Testing Detail Movie
   * request params : imdbID: "tt1673430", title : "Batman", page : 2
   * response: 400
   * response json : { code : 400, message: "Data not found", data: null}
   */
  test("400 - Data not found", (done) => {
    request(app)
      .get(`/movies/detail?imdbID=xx123`)
      .send()
      .set('Accept', 'application/json')
      .then(response => {
        const { body, status } = response
        expect(status).toBe(200)
        expect(body).toHaveProperty("code", 400,)
        expect(body).toHaveProperty("message", "Data not found")
        expect(body).toHaveProperty("data", null)

        done()

      })
      .catch(e => done(e))
  })
})
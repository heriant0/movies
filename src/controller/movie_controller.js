const utils = require('../helpers/response')
const services = require('../services/movie_service')

class Movie {

  static async searchMovie(req, res, next) {
    let { title } = req.query
    let url = req.url

    try {

      let result = await services.searchMovies(title, url)

      if (!result) {
        throw {
          code: 400,
          name: "CustomValidation",
          message: "Data not found",
          data: null
        }
      } else {
        return utils.response(res, next, 200, "Succesfully get data", result)
      }

    } catch (e) {
      next(e)
    }
  }

  static async detailMovie(req, res, next) {

    let { imdbID } = req.query
    let url = req.url

    try {
      let result = await services.detailMovies(imdbID, url)

      if (result == false) {
        throw {
          code: 400,
          name: "CustomValidation",
          message: "Data not found",
          data: null
        }
      } else {
        return utils.response(res, next, 200, "Succesfully get data", result)
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Movie


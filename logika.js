
function anagram(arrWords) {
  let result = {};
  for (let word of arrWords) {

    let splitted = word.split("")
    let sorted = splitted.sort().join("")

    if (result[sorted]) {
      result[sorted].push(word);
    } else {
      result[sorted] = [word];
    }
  }
  return Object.values(result);
}

let arrWords = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']
let result = anagram(arrWords)
console.log(result)


// Refactor function
function findFirstStringInBracket(str) {

  if (str.length == 0) return ""
  
  let indexFirstBracketFound
  let wordsAfterFirstBracket
  let indexClosingBracketFound

  indexFirstBracketFound = str.indexOf("(");

  if (indexFirstBracketFound >= 0) {
    wordsAfterFirstBracket = str.substr(indexFirstBracketFound);
    indexClosingBracketFound = wordsAfterFirstBracket.lastIndexOf(")");

    if (!wordsAfterFirstBracket || indexClosingBracketFound < 0) {
      return ""
    } else {
      return wordsAfterFirstBracket.substring(1, indexClosingBracketFound);
    }
  } else {
    return ""
  }

}

let text = findFirstStringInBracket("dua(puluh)tahun")
console.log("RESULT:", text)